package com.otp.notification.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.otp.notification.audit.Auditable;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.Date;

@Data
@Entity
@Table(name = "t_otp_request")
public class OTPRequest extends Auditable<String> {

    @Column(name = "customer_code", length = 20, unique = true)
    private String customerCode;

    @Column(name = "customer_name", length = 50)
    private String customerName;

    @Column(name = "phone_no", length = 20)
    private String phoneNo;

    @Column(name = "email", length = 50)
    private String email;

    @Column(name = "gender_code", length = 2)
    private String genderCode;

    @Column(name = "otp_type", length = 10)
    private String otpType;

    @Column(name = "type_code", length = 10)
    private String typeCode;

    @NotEmpty(message = "OTP Code may not be empty")
    @Column(unique = true, name = "otp_code", length = 6)
    private String otpCode;

    @JsonFormat(pattern="yyyy-MM-dd")
    @Column(name = "expired_time")
    private Date expiredTime;

    @Column(name = "sent_date")
    private String sentDate;

    @Column(name = "status")
    private boolean status;
}
