package com.otp.notification.repository;

import com.otp.notification.model.OTPRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface OTPRequestRepository extends JpaRepository<OTPRequest, Long> {
//    @Query("select rh from RequestHistory rh where rh.code = ?1")
//    RequestHistory findByOtp(String code);

    @Query("select ro from OTPRequest ro where ro.otpCode = ?1")
    OTPRequest findByOtpCode(String otp);
}
